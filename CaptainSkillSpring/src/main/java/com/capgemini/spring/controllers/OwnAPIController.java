package com.capgemini.spring.controllers;


import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Own;
import com.capgemini.spring.models.Skill;
import com.capgemini.spring.models.User;


/**
 * OwnApiController class
 * 
 */
@RestController
@RequestMapping("/own")
public class OwnAPIController {
	/**
	 * Manager of Own
	 * 
	 */
	@Autowired
	private IBaseManager<Own> manager;

	/**
	 * Manager of User
	 * 
	 */
	@Autowired
	private IBaseManager<User> managerUser;
	
	/**
	 * Manager of Skill
	 * 
	 */
	@Autowired
	private IBaseManager<Skill> skillManager;
	
	/**
	 * List of all owns
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Own> getAll() {
		return this.manager.getAll();
	}
	
	/**
	 * Get a own by id
	 * 
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Own get(@PathVariable Integer id, HttpServletResponse response) {
		Own entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}

		return entity;
	}
	
	/**
	 * Delete a own
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Own delete(@PathVariable Integer id) {
		Own entity = this.manager.getById(id);

		if (entity != null) {
			this.manager.delete(entity);
		}

		return entity;
	}
	
	/**
	 * Create a own
	 * 
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Own create(@RequestParam Short level, @RequestParam Short validation, @RequestParam String message, @RequestParam Integer userId, 
			@RequestParam Integer skillId) {
		User user = this.managerUser.getById(userId);
		Skill skill = this.skillManager.getById(skillId);
		Own entity = new Own(level, validation, message, user, skill);
	
		this.manager.create(entity);
		return entity;
	}
	
	/**
	 * Update a own
	 * 
	 * @param
	 * @return
	 */
	@PutMapping("/{id}")
	public Own update(HttpServletResponse response, @PathVariable int id, @RequestParam int level,
			@RequestParam(required=false) Short validation, @RequestParam(required=false) String message) {


		Own entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} else {
				entity.setLevel((short)level);

			if (validation != null) {
				entity.setValidation(validation);
			}
			
			if (message != null) {
				entity.setMessage(message);
			}

			this.manager.update(entity);
		}
		return entity;
	}
	
    
    /**
     * Associate a skill to own
     * 
     * @param
     * @param
     * @return
     */
    @RequestMapping(value="/addSkilltoOwn/{skillId}/{ownId}", method=RequestMethod.GET)
    public Own addSkill(@PathVariable Integer ownId, @PathVariable Integer skillId){
    	Own own = this.manager.getById(ownId);
    	Skill skill = this.skillManager.getById(skillId);
    	own.setSkill(skill);
    	this.manager.update(own);
    	return own;
    }
    
    /**
     * Associate a user to own
     * 
     * @param
     * @param
     * @return
     */
    @RequestMapping(value="/addUsertoOwn/{userId}/{ownId}", method=RequestMethod.GET)
    public Own addUser(@PathVariable Integer userId, @PathVariable Integer ownId){
    	Own own = this.manager.getById(ownId); 
    	User user = this.managerUser.getById(userId);
    	own.setUser(user);
    	this.manager.update(own);

    	return own;
    }

    /**
     * Get owns from one user
     * 
     * @param userId
     * @return
     */
    @RequestMapping(value="/user/{userId}", method=RequestMethod.GET)
    public  List<Own> getByUserId(@PathVariable Integer userId){
    	return this.manager.getByUserId(userId);
    }
    

}
