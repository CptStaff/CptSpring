package com.capgemini.spring.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.GeneralSkillType;
import com.capgemini.spring.models.SkillType;

/**
 * SkillTypeApiController class
 * 
 */
@RestController
@RequestMapping("/skill_types")
public class SkillTypeAPIController {
    @Autowired
    private IBaseManager<SkillType> manager;
    
    @Autowired
    private IBaseManager<GeneralSkillType> managerGeneral;

	/**
	 * List of all skill types
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<SkillType> getAll() {
    	List<SkillType> skills = this.manager.getAll();
    	skills.sort((c1,c2) -> c1.getId().compareTo(c2.getId()));
        return skills ;
    }

	/**
	 * Get a skill type by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public SkillType get(@PathVariable Integer id, HttpServletResponse response) {
        SkillType entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public SkillType delete(@PathVariable Integer id) {
        SkillType type = this.manager.getById(id);

        if (type != null) {
            this.manager.delete(type);
        }

        return type;
    }

	/**
	 * Create a skill type with a general skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)
    public SkillType create(@RequestParam String name, @RequestParam Integer skillGTId) {
        SkillType entity = new SkillType();
        GeneralSkillType generalSkillType = this.managerGeneral.getById(skillGTId);

        entity.setName(name);
        entity.setGeneralSkillType(generalSkillType);

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a skill type
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public SkillType update(HttpServletResponse response, @PathVariable int id, @RequestParam String name) {
        SkillType entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(name != null) {
            entity.setName(name);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }

  
  
    /**
     * Give SkillType a GeneralSkillType
     * 
     * @param skillTypeId
     * @param generalSkillTypeId
     * @return
     */
    @RequestMapping(value="/{skillTypeId}/{generalSkillTypeId}", method=RequestMethod.GET)
    public SkillType addSkillType(@PathVariable Integer skillTypeId, @PathVariable Integer generalSkillTypeId){
    	SkillType skillType = this.manager.getById(skillTypeId);
    	GeneralSkillType generalSkillType = this.managerGeneral.getById(generalSkillTypeId);
    	skillType.setGeneralSkillType(generalSkillType);
    	this.manager.update(skillType);
    	return skillType;
    }

    /**
     * show GenralSkillType by SkillType
     * 
     * @param skillTypeId
     * @return
     */
    @RequestMapping(value="/GST/{skillTypeId}", method=RequestMethod.GET)
    public GeneralSkillType getGeneralSkillType(@PathVariable Integer skillTypeId) {
    	SkillType skillType = this.manager.getById(skillTypeId);
    	
    	GeneralSkillType generalSkillType = skillType.getGeneralSkillType();
    	return generalSkillType;
    }
}
