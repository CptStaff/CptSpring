package com.capgemini.spring.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.managers.UserManager;
import com.capgemini.spring.managers.interfaces.base.IBaseManager;

import com.capgemini.spring.models.Status;
import com.capgemini.spring.models.User;

/**
 * UserAPIController class
 * 
 */
@RestController
@RequestMapping("/user")
public class UserAPIController {
    @Autowired
    private UserManager manager;
    
    @Autowired
    private IBaseManager<Status> managerStatus;

	/**
	 * List of all users
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<User> getAll() {
        return this.manager.getAll();
    }

	/**
	 * Get a user by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public User get(@PathVariable Integer id, HttpServletResponse response) {
        User entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a user
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public User delete(@PathVariable Integer id) {
        User type = this.manager.getById(id);

        if (type != null) {
            this.manager.delete(type);
        }

        return type;
    }

	/**
	 * Create a user
	 * 
	 * @param
	 */
    @RequestMapping(value="/", method=RequestMethod.POST)

    public User create(@RequestParam String lastname,
    		@RequestParam String firstname, @RequestParam String email, @RequestParam String password,
    		@RequestParam short right) {
        User entity = new User();
        
        entity.setLastname(lastname);
        entity.setFirstname(firstname);
        entity.setEmail(email);
        entity.setPassword(password);
        entity.setRight(right);
        

        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a user
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public User update(HttpServletResponse response, @PathVariable int id,
    		@RequestParam(required=false) String lastname,
    		@RequestParam(required=false) String firstname,
    		@RequestParam(required=false) int right) {
        User entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(lastname != null && !lastname.equals(entity.getLastname())) {
            entity.setLastname(lastname);

            this.manager.update(entity);
        } else if (right!=(entity.getRight())){
        	entity.setRight(right);
        	
        	this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }


    
    /**
     * 
     * Give user a status
     * @param userId
     * @param statusId
     * @return
     */
    @RequestMapping(value="/addStatusToUser/{userId}/{statusId}", method=RequestMethod.GET)
    public Status addStatus(@PathVariable Integer userId, @PathVariable Integer statusId){
    	User user = this.manager.getById(userId);
    	Status status = this.managerStatus.getById(statusId);
    	user.setStatus(status);
    	this.manager.update(user);
    	return status;
    }

    /**
     * Return the user by his mail for log in
     * 
     * @param response
     * @param email
     * @param password
     * @return
     */
    @GetMapping("/login")
    public User connectionAction(HttpServletResponse response, @RequestParam String email, @RequestParam String password) {
    	User user = this.manager.getByEmail(email);
    	// TODO Add security.
    	if (user == null || user.getPassword() == null || !user.getPassword().equals(password)) {
    		response.setStatus(500);
    		return null;
    	}
    	
    	return user;
    }

}
