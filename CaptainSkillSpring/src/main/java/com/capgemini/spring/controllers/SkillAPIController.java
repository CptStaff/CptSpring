package com.capgemini.spring.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Skill;
import com.capgemini.spring.models.SkillType;
import com.capgemini.spring.models.Status;


/**
 * SkillAPIController class
 * 
 */
@RestController
@RequestMapping("/skill")
public class SkillAPIController {
    @Autowired
    private IBaseManager<Skill> manager;
    @Autowired
    private IBaseManager<SkillType> managerType;
    @Autowired
    private IBaseManager<Status> managerStatus;

	/**
	 * List of all skills
	 * 
	 */
    @RequestMapping(value="/", method=RequestMethod.GET)
    public List<Skill> getAll() {
        return this.manager.getAll();
    }

	/**
	 * Get a skill by id
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Skill get(@PathVariable Integer id, HttpServletResponse response) {
        Skill entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }

        return entity;
    }

	/**
	 * Delete a skill
	 * 
	 * @param
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    public Skill delete(@PathVariable Integer id) {
        Skill type = this.manager.getById(id);

        if (type != null) {
            this.manager.delete(type);
        }

        return type;
    }

	/**
	 * Create a skill (with skillType)
	 * 
	 * @param
	 */   
    @RequestMapping(value="/", method=RequestMethod.POST)
    public Skill create(@RequestParam String name, @RequestParam Integer skillTId) {
        Skill entity = new Skill();
        SkillType skillType = this.managerType.getById(skillTId);
        
        entity.setName(name);
        entity.setSkillType(skillType);

        
        this.manager.create(entity);

        return entity;
    }

	/**
	 * Update a skill
	 * 
	 * @param
	 */
	@PutMapping("/{id}/")
	public Skill update(HttpServletResponse response, @PathVariable int id, 
			@RequestParam String name, @RequestParam Integer StatusId, @RequestParam Integer SkillTypeId) {
		Skill entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} else {
			if (name != null && !name.equals(entity.getName())){
				entity.setName(name);
			}
			
			if (StatusId != null && !(StatusId == (entity.getStatus().getId()))){
				Status status = this.managerStatus.getById(StatusId);
				entity.setStatus(status);
				
			}
			
			if (SkillTypeId != null && !SkillTypeId.equals(entity.getSkillType().getId())){
				SkillType skillType = this.managerType.getById(SkillTypeId);
				entity.setSkillType(skillType);
			}		
			
			this.manager.update(entity);
		} 
		return entity;
	}

    
    /**
     * Give Skill a SkillType
     * 
     * @param skillId
     * @param skillTypeId
     * @return
     */
    @RequestMapping(value="/addSTToSkill/{skillId}/{skillTypeId}", method=RequestMethod.GET)
    public SkillType addSkillType(@PathVariable Integer skillId, @PathVariable Integer skillTypeId){
    	Skill skill = this.manager.getById(skillId);
    	SkillType skillType = this.managerType.getById(skillTypeId);
    	skill.setSkillType(skillType);
    	this.manager.update(skill);
    	return skillType;
    }
    
    /**
     * Give Skill a Status
     * 
     * @param skillId
     * @param statusId
     * @return
     */
    @RequestMapping(value="/addStatusToSkill/{skillId}/{statusId}", method=RequestMethod.GET)
    public Status addStatus(@PathVariable Integer skillId, @PathVariable Integer statusId){
    	Skill skill = this.manager.getById(skillId);
    	Status status = this.managerStatus.getById(statusId);
    	skill.setStatus(status);
    	this.manager.update(skill);
    	return status;
    }
    

    
    
    /**
     * show skillType by skill
     * 
     * @param skillId
     * @return
     */
    @RequestMapping(value="/ST/{skillId}", method=RequestMethod.GET)
    public SkillType getSkillType(@PathVariable Integer skillId) {
    	Skill skill = this.manager.getById(skillId);
    	
    	SkillType skillType = skill.getSkillType();
    	return skillType;
    }
    
}
