package com.capgemini.spring.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Project;


/**
 * ProjectApiController class
 * 
 */
@RestController
@RequestMapping("/project")
public class ProjectAPIController {
	/**
	 * Manager of Project
	 * 
	 */
	@Autowired
	private IBaseManager<Project> manager;
	
	/**
	 * List of projects
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Project> getAll() {
		return this.manager.getAll();
	}
	
	/**
	 * Get a project
	 * 
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Project get(@PathVariable Integer id, HttpServletResponse response) {
		Project entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}

		return entity;
	}
	
	/**
	 * Delete a project
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Project delete(@PathVariable Integer id) {
		Project entity = this.manager.getById(id);

		if (entity != null) {
			this.manager.delete(entity);
		}

		return entity;
	}
	
	/**
	 * Create a project
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Project create(@RequestParam String name) {
		Project entity = new Project();

		entity.setName(name);
		
		this.manager.create(entity);

		return entity;
	}
	
	/**
	 * Update a project
	 * 
	 * @param
	 * @return
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public Project update(HttpServletResponse response, @PathVariable int id, @RequestParam String name) {
        Project entity = this.manager.getById(id);

        if (entity == null) {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        } else if(name != null && !name.equals(entity.getName())) {
            entity.setName(name);

            this.manager.update(entity);
        } else {
            response.setStatus(418);
        }

        return entity;
    }
    	
}
