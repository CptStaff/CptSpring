package com.capgemini.spring.controllers;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Skill;
import com.capgemini.spring.models.Status;

/**
 * StatusApiController class
 * 
 */
@RestController
@RequestMapping("/status")
public class StatusAPIController {
	/**
	 * Manager of Status
	 * 
	 */
	@Autowired
	private IBaseManager<Status> manager;
	private IBaseManager<Skill> managerSkill;
	
	/**
	 * Manager of Skill
	 * 
	 */
	@Autowired
	private IBaseManager<Skill> skillManager;
	
	/**
	 * List of status
	 * 
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Status> getAll() {
		return this.manager.getAll();
	}
	
	/**
	 * Get a status by id
	 * 
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Status get(@PathVariable Integer id, HttpServletResponse response) {
		Status entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}

		return entity;
	}
	
	/**
	 * Delete a status
	 * 
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public Status delete(@PathVariable Integer id) {
		Status entity = this.manager.getById(id);

		if (entity != null) {
			this.manager.delete(entity);
		}

		return entity;
	}
	
	/**
	 * Create a status
	 * 
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Status create(@RequestParam String name, @RequestParam Integer year, @RequestParam Integer month,
			@RequestParam Integer day) {
		Status entity = new Status(name);
		LocalDate dateEnd = LocalDate.of(year, month, day);
		entity.setDateEnd(dateEnd);
		
		this.manager.create(entity);
		return entity;
	}
	
	/**
	 * Update a status
	 * 
	 * @param
	 * @param
	 * @param
	 * @param
	 * @param
	 * @return
	 */
	@PutMapping("/{id}")
	public Status update(HttpServletResponse response, @PathVariable int id, 
			@RequestParam String name, @RequestParam LocalDate dateEnd, @RequestBody List<Skill> skills) {
		Status entity = this.manager.getById(id);

		if (entity == null) {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		} else {
			if (name != null && !name.equals(entity.getName())){
				entity.setName(name);
			}
			
			if (dateEnd != null && !dateEnd.equals(entity.getDateEnd())){
				entity.setDateEnd(dateEnd);
			}
			
			if (skills != null && !skills.equals(entity.getSkills())){
				entity.setSkills(skills);
			}		
			this.manager.update(entity);
		} 
		return entity;
	}
	
  
}
