package com.capgemini.spring.configuration.security.service;

/**
 * SecurityService interface
 * 
 */
public interface SecurityService {
    String findLoggedInUsername();

    void autologin(String username, String password);
}