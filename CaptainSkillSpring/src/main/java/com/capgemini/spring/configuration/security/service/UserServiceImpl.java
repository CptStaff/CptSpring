package com.capgemini.spring.configuration.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.spring.configuration.security.dao.ISecurityRoleCrudRepository;
import com.capgemini.spring.configuration.security.dao.ISecurityUserCrudRepository;
import com.capgemini.spring.configuration.security.models.SecurityRole;
import com.capgemini.spring.configuration.security.models.SecurityUser;

import java.util.Set;

/**
 * UserServiceImpl class
 * 
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private ISecurityUserCrudRepository userRepository;
    @Autowired
    private ISecurityRoleCrudRepository roleRepository;

    @Override
    public void save(SecurityUser user) {
        user.setPassword(user.getPassword());
        user.setRoles((Set<SecurityRole>)roleRepository.findAll());
        userRepository.save(user);
    }

    @Override
    public SecurityUser findByLogin(String login) {
        return userRepository.findByLogin(login);
    }
}
