package com.capgemini.spring.configuration.security.dao;

import org.springframework.data.repository.CrudRepository;

import com.capgemini.spring.configuration.security.models.SecurityRole;


/**
 * UserServiceImpl interface
 * 
 * @extends
 */
public interface ISecurityRoleCrudRepository extends CrudRepository<SecurityRole, Integer> {

}
