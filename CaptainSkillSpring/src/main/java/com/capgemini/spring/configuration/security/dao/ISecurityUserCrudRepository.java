package com.capgemini.spring.configuration.security.dao;

import org.springframework.data.repository.CrudRepository;

import com.capgemini.spring.configuration.security.models.SecurityUser;

/**
 * UserDetailsServiceImpl interface
 * 
 * @extends
 */
public interface ISecurityUserCrudRepository extends CrudRepository<SecurityUser,Integer> {
	SecurityUser findByLogin(String login);
}
