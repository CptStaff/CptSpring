package com.capgemini.spring.configuration.security;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * SecurityConfig class
 * 
 * @extends
 */
@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter  {

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity
				.csrf().disable()	//pour retirer sécurité
				.authorizeRequests()
				.anyRequest()
				.permitAll()
//				.authenticated()
//			.and()
//				.formLogin()
//					.loginPage("/login")
//					.usernameParameter("username").passwordParameter("password")
//					.permitAll()
//			.and()
//        		.httpBasic()
//        	.and()
//        		.authorizeRequests().anyRequest().anonymous()
//        		.antMatchers("/api","/api/**")
//        		.permitAll()
        	;
	}
}
