package com.capgemini.spring.configuration.security.service;

import com.capgemini.spring.configuration.security.models.SecurityUser;

/**
 * UserServiceImpl interface
 * 
 */
public interface UserService {
    void save(SecurityUser user);

    SecurityUser findByLogin(String login);
}
