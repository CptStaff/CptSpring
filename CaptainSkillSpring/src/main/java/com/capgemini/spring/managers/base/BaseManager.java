package com.capgemini.spring.managers.base;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.base.BaseEntity;

/**
 * BaseManager class
 * 
 * @abstract
 * @extends
 * @implements
 */
@Service
public abstract class BaseManager <T extends BaseEntity> implements IBaseManager<T>{
	/**
	 * IBaseDAO interface
	 * 
	 */
	@Autowired
	private IBaseDAO<T> dao;

	/**
	 * Create an item
	 * 
	 * @param
	 */
	@Override
	public void create(T item) {
		dao.create(item);
	}

	/**
	 * Delete an item
	 * 
	 * @param
	 */
	@Override
	public void delete(T item) {
		dao.delete(item);
	}

	/**
	 * List of all items
	 * 
	 */
	@Override
	public List<T> getAll() {
		return dao.getAll();
	}

	/**
	 * Get item by id
	 * 
	 * @param
	 */
	@Override
	public T getById(Integer id) {
		return dao.getById(id);
	}

	/**
	 * Update an item
	 * 
	 * @param
	 */
	@Override
	public void update(T item) {
		dao.update(item);
	}

	
	/** Gets the dao. */
	protected IBaseDAO<T> getDao() {
		return this.dao;
	}
	

	public List<T> getByUserId(Integer userId){
		return dao.getByUserId(userId);
	};
}
