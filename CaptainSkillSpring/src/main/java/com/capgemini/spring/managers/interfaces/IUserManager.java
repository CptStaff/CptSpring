package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.User;

/**
 * IUserManager interface
 * 
 * @extends
 */
public interface IUserManager extends IBaseManager<User> {

}
