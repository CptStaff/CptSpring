package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.IOwnManager;
import com.capgemini.spring.models.Own;

/**
 * SkillManager class
 * 
 * @extends
 * @implements
 */
public class OwnManager extends BaseManager<Own> implements IOwnManager{

}
