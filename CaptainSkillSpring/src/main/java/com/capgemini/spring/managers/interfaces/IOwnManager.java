package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Own;

/**
 * IOwnManager interface
 * 
 * @extends
 */
public interface IOwnManager extends IBaseManager<Own> {

}
