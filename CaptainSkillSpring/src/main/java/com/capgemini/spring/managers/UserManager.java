package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.IUserManager;
import com.capgemini.spring.models.User;

/**
 * UserManager class
 * 
 * @extends
 * @implements
 */
public class UserManager extends BaseManager<User> implements IUserManager{

	
	public User getByEmail(String email) {
		return (User) this.getDao().createQuery("SELECT u FROM User u WHERE u.email=:email").setParameter("email", email).setMaxResults(1).getSingleResult();
	}
	
}
