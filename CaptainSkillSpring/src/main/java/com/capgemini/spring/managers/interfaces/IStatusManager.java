package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Status;

/**
 * IStatusManager interface
 * 
 * @extends
 */
public interface IStatusManager extends IBaseManager<Status> {

}
