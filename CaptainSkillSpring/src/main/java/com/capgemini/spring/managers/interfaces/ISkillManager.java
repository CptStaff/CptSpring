package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Skill;

/**
 * ISkillManager interface
 * 
 * @extends
 */
public interface ISkillManager extends IBaseManager<Skill>{

}
