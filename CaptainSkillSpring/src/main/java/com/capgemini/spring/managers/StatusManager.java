package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.IStatusManager;
import com.capgemini.spring.models.Status;

/**
 * StatusManager class
 * 
 * @extends
 * @implements
 */
public class StatusManager extends BaseManager<Status> implements IStatusManager{

}
