package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.GeneralSkillType;

/**
 * IGeneralSkillTypeManager interface
 * 
 * @extends
 */
public interface IGeneralSkillTypeManager extends IBaseManager<GeneralSkillType> {

}
