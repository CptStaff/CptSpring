package com.capgemini.spring.managers;

import java.util.List;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.IGeneralSkillTypeManager;
import com.capgemini.spring.models.GeneralSkillType;

/**
 * GeneralSkillTypeManager class
 * 
 * @extends
 * @implements
 */
public class GeneralSkillTypeManager extends BaseManager<GeneralSkillType> implements IGeneralSkillTypeManager{

}
