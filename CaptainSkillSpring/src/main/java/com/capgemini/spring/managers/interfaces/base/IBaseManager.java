package com.capgemini.spring.managers.interfaces.base;

import java.util.List;

import com.capgemini.spring.models.Own;
import com.capgemini.spring.models.base.BaseEntity;

/**
 * IBaseManager interface
 * 
 * @extends
 */
public interface IBaseManager<T extends BaseEntity> {

	/**
	 * Create an item
	 * 
	 * @param
	 */
	public void create(T item);

	/**
	 * Delete an item
	 * 
	 * @param
	 */
	public void delete(T item);

	/**
	 * List of all items
	 * 
	 */
	public List<T> getAll();

	/**
	 * Get item by id
	 * 
	 * @param
	 */
	public T getById(Integer id);

	/**
	 * Update an item
	 * 
	 * @param
	 */
	public void update(T item);
	
	public List<T> getByUserId(Integer userId);
}
