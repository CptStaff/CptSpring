package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.IProjectManager;
import com.capgemini.spring.models.Project;


/**
 * ProjectManager class
 * 
 * @extends
 * @implements
 */
public class ProjectManager extends BaseManager<Project> implements IProjectManager{

}
