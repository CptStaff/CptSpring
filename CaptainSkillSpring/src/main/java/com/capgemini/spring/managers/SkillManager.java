package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.ISkillManager;
import com.capgemini.spring.models.Skill;

/**
 * SkillManager class
 * 
 * @extends
 * @implements
 */
public class SkillManager extends BaseManager<Skill> implements ISkillManager{

}
