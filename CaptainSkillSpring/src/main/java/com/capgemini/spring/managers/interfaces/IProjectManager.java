package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.Project;

/**
 * IProjectManager interface
 * 
 * @extends
 */
public interface IProjectManager extends IBaseManager<Project>{

}
