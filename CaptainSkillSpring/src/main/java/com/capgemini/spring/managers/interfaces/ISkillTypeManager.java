package com.capgemini.spring.managers.interfaces;

import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.SkillType;

/**
 * ISkillTypeManager interface
 * 
 * @extends
 */
public interface ISkillTypeManager extends IBaseManager<SkillType> {

}
