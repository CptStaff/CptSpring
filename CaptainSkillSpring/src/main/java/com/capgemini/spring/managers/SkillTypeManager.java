package com.capgemini.spring.managers;

import com.capgemini.spring.managers.base.BaseManager;
import com.capgemini.spring.managers.interfaces.ISkillTypeManager;
import com.capgemini.spring.models.SkillType;

/**
 * SkillTypeManager class
 * 
 * @extends
 * @implements
 */
public class SkillTypeManager extends BaseManager<SkillType> implements ISkillTypeManager{

}
