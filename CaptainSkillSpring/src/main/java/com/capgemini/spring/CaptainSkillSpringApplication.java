package com.capgemini.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.capgemini.spring.dao.GeneralSkillTypeDAO;
import com.capgemini.spring.dao.OwnDAO;
import com.capgemini.spring.dao.ProjectDAO;
import com.capgemini.spring.dao.SkillDAO;
import com.capgemini.spring.dao.SkillTypeDAO;
import com.capgemini.spring.dao.StatusDAO;
import com.capgemini.spring.dao.UserDAO;
import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.managers.GeneralSkillTypeManager;
import com.capgemini.spring.managers.OwnManager;
import com.capgemini.spring.managers.ProjectManager;
import com.capgemini.spring.managers.SkillManager;
import com.capgemini.spring.managers.SkillTypeManager;
import com.capgemini.spring.managers.StatusManager;
import com.capgemini.spring.managers.UserManager;
import com.capgemini.spring.managers.interfaces.base.IBaseManager;
import com.capgemini.spring.models.GeneralSkillType;
import com.capgemini.spring.models.Own;
import com.capgemini.spring.models.Project;
import com.capgemini.spring.models.Skill;
import com.capgemini.spring.models.SkillType;
import com.capgemini.spring.models.Status;
import com.capgemini.spring.models.User;

/**
 * Application class
 * 
 */
@SpringBootApplication
public class CaptainSkillSpringApplication {
	/**
	 * Application entry point
	 * 
	 */
	@Bean
	   public WebMvcConfigurer corsConfigurer() {
	       return new WebMvcConfigurerAdapter() {
	           @Override
	           public void addCorsMappings(CorsRegistry registry) {
	               registry.addMapping("/**/*")
	               		.allowedMethods("HEAD", "PUT", "DELETE", "GET", "POST")
	               		.allowedHeaders("Content-Type")
	               		.allowedOrigins("http://localhost:4200")
	               		.allowCredentials(false)
               		;
	           }
	       };
	   }
	
	public static void main(String[] args) {
		SpringApplication.run(CaptainSkillSpringApplication.class, args);
	}
	/**
	 * Treatment of a general skill type
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<GeneralSkillType> getGeneralSkillTypeManager(){
		return new GeneralSkillTypeManager();
	}

	/**
	 * Import of a general skill type from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<GeneralSkillType> getGeneralSkillTypeDAO(){
		return new GeneralSkillTypeDAO();
	}
	/**
	 * Treatment of a skill type
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<SkillType> getSkillTypeManager(){
		return new SkillTypeManager();
	}

	/**
	 * Import of a skill type from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<SkillType> getSkillTypeDAO(){
		return new SkillTypeDAO();
	}
	
	/**
	 * Treatment of a skill
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<Skill> getSkillManager(){
		return new SkillManager();
	}

	/**
	 * Import of a skill from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<Skill> getSkillDAO(){
		return new SkillDAO();
	}
	
	/**
	 * Treatment of a user
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<User> getUserManager(){
		return new UserManager();
	}

	/**
	 * Import of a user from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<User> getUserDAO(){
		return new UserDAO();
	}
	
	/**
	 * Treatment of a own
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<Own> getOwnManager() {
		return new OwnManager();
	}

	/**
	 * Import of a own from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<Own> getOwnDAO() {
		return new OwnDAO();
	}
	
	/**
	 * Treatment of a status
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<Status> getStatusManager() {
		return new StatusManager();
	}

	/**
	 * Import of a status from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<Status> getStatusDAO() {
		return new StatusDAO();
	}
	
	/**
	 * Treatment of a project
	 * 
	 * @return
	 */
	@Bean
	public IBaseManager<Project> getProjectManager() {
		return new ProjectManager();
	}

	/**
	 * Import of a project from database
	 * 
	 * @return
	 */
	@Bean
	public IBaseDAO<Project> getProjectDAO() {
		return new ProjectDAO();
	}



}
