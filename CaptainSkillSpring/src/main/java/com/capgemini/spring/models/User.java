package com.capgemini.spring.models;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import javax.persistence.Table;



import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * User class
 * 
 * @extends
 */
@Entity
@Table(name = "user")
public class User extends BaseEntity{
	
	@Column(name = "user_lastname", length=150)
	@Length(min=2, max=150)
	private String lastname;
	
	@Column(name = "user_firstname", length=150)
	@Length(min=2, max=150)
	private String firstname;
	
	@Column(length=150, unique=true)
	@Length(min=2, max=150)
	private String email;
	
	@Column(name = "user_password")
	@Length(min=2, max=150)
	private String password;
	
	@Column(name = "user_right")
	@Range(min=0 , max=2)
	private short right;

	@ManyToOne
	@JoinColumn(name="status_id")
	private Status status;
	
	private String token;


	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public User() {

	}

	public User(String lastname, String firstname, String email, String password, Short right/*, Status status*/) {
        this.setLastname(lastname);
        this.setFirstname(firstname);
        this.setEmail(email);
        this.setPassword(password);
        this.setRight(right);
        //this.setStatus(status);
	}

	public short getRight() {
		return right;
	}

	public void setRight(int right2) {
		this.right = (short) right2;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [lastname=" + lastname + ", firstname=" + firstname
				+ ", email=" + email + ", password=" + password + ", getId()="
				+ getId() + "]";
	}
}
