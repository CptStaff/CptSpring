package com.capgemini.spring.models;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Range;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * Own class
 * 
 * @extends
 */
@Entity
@Table(name = "own")
public class Own extends BaseEntity{
	@Column(name = "own_level")
	@Range(min=0, max=5)
	private Short level;
	
	@Column(name = "own_validation")
	@Range(min=0, max=5)
	private Short validation;
	
	@Column(name="own_message")
	private String message;
	
	@OneToOne
	private User user;

	@OneToOne
	private Skill skill;

	public Short getValidation() {
		return validation;
	}

	public void setValidation(Short validation) {
		this.validation = validation;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

public Own() {
		
	}

	
	public Own(Short level, Short validation, String message) {
		this.setLevel(level);
		this.setValidation(validation);
		this.setMessage(message);
	}
	
	public Own(Short level, Short validation, String message, User user, Skill skill) {
		this.setLevel(level);
		this.setValidation(validation);
		this.setMessage(message);
		this.setUser(user);
		this.setSkill(skill);
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(Short level) {
		this.level = level;
	}

	public Skill getSkill() {
		return skill;
	}

	public void setSkill(Skill skill) {
		this.skill = skill;
	}
}
