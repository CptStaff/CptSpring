package com.capgemini.spring.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * GeneralSkillType class
 * 
 * @extends
 */
@Entity
@Table(name = "general_skill_type")
public class GeneralSkillType extends BaseEntity {
	@Column(name="general_skill_type_name", length=150, unique=true)
	@Length(min=2, max=150)
    private String name;
	
	
	public GeneralSkillType(){		
	}
	
	public GeneralSkillType(String name) {
		this.setName(name);
	}

    public GeneralSkillType(Integer id, String name) {
        super(id);
        this.setName(name);
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
