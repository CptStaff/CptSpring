package com.capgemini.spring.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.capgemini.spring.models.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Status class
 * 
 * @extends
 */
@Entity
@Table(name = "status")
public class Status extends BaseEntity {
	@Column(name = "status_name", length=150, unique=true)
	@Length(min=2, max=150)
	private String name;

	@Column(name = "status_dateEnd")
	private LocalDate dateEnd;
	

	@OneToMany( mappedBy="status")
	@JsonIgnoreProperties("status")
	private List<Skill> skills;
 	

	public Status() {

	}
	
	public Status(String name) {
		this.setName(name);
	}

	public Status(String name, LocalDate dateEnd) {
		this.setName(name);
		this.setDateEnd(dateEnd);
	}
	
	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}

	public String getName() {
		return name;
	}

	public void setName(String statusName) {
		this.name = statusName;
	}

	public LocalDate getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDate statusDateEnd) {
		this.dateEnd = statusDateEnd;
	}
}
