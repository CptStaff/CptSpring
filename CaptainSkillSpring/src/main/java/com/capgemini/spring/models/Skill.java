package com.capgemini.spring.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.capgemini.spring.models.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.capgemini.spring.models.SkillType;

/**
 * Skill class
 * 
 * @extends
 */
@Entity
@Table(name = "skill")
public class Skill extends BaseEntity{
	@Column(name = "skill_name", length=150, unique=true)
	@Length(min=1, max=150)
	private String name;
	
	@ManyToOne
	@JoinColumn(name="skill_type_id")
	private SkillType skillType;
	
	//indispensable pour le oneToMany de status vers skill
	@ManyToOne
	@JoinColumn(name="status_id")
	@JsonIgnoreProperties("skill")
	private Status status;
	

	public Skill() {
		
    }
    
    public Skill(String name) {
        this.setName(name);
    }
    
    public Skill(String name, SkillType skillType) {
    	this.setName(name);
    	this.setSkillType(skillType);
    }
	
    
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public SkillType getSkillType() {
		return skillType;
	}
	
	public void setSkillType(SkillType skillType) {
		this.skillType = skillType;
	}
}
