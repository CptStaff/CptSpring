package com.capgemini.spring.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * Project class
 * 
 * @extends
 */
@Entity
@Table
public class Project extends BaseEntity{
	
	@Column(name = "project_name", length=150, unique=true)
	@Length(min=2, max=150)
    private String name;
	
	
	public Project(){		
	}
	
	public Project(String name) {
		this.setName(name);
	}

    public Project(Integer id, String name) {
        super(id);
        this.setName(name);
    }
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
