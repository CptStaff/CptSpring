package com.capgemini.spring.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Length;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * SkillType class
 * 
 * @extends
 */
@Entity
@Table(name = "skill_type")
public class SkillType extends BaseEntity {
	@Column(name="skill_type_name", length=150, unique=true)
	@Length(min=2, max=150)
    private String name;
	
	@ManyToOne
	@JoinColumn(name="general_skill_type_id")
	private GeneralSkillType generalSkillType;
	
    public SkillType() {
    }
    
    public SkillType(String name) {
        this.setName(name);
    }
    
    public SkillType(Integer id, String name) {
        super(id);
        this.setName(name);
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public GeneralSkillType getGeneralSkillType() {
		return generalSkillType;
	}

	public void setGeneralSkillType(GeneralSkillType generalSkillType) {
		this.generalSkillType = generalSkillType;
	}
	


}
