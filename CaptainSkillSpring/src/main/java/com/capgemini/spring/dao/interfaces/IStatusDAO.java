package com.capgemini.spring.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.models.Status;

/**
 * IStatusDAO interface
 * 
 * @extends
 */
@Repository
public interface IStatusDAO extends IBaseDAO<Status>{

}
