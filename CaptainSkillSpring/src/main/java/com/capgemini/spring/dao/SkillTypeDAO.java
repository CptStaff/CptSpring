package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.ISkillTypeDAO;
import com.capgemini.spring.models.SkillType;

/**
 * SkillTypeDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class SkillTypeDAO extends BaseDAO<SkillType> implements ISkillTypeDAO {
	/**
	 * Create a skill type
	 * 
	 * @param
	 */
	@Override
	public void create(SkillType item) {
		entityManager.persist(item);		
	}

	/**
	 * Delete a skill type
	 * 
	 * @param
	 */
	@Override
	public void delete(SkillType item) {
		entityManager.remove(item);
	}
	
	/**
	 * List of all skill types
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<SkillType> getAll() {
		return entityManager.createQuery("SELECT st FROM SkillType st ORDER BY id ASC").getResultList();
	}

	/**
	 * Get a skill type by id
	 * 
	 * @param
	 */
	@Override
	public SkillType getById(Integer id) {
		return entityManager.find(SkillType.class, id);
	}

	/**
	 * Update a skill type
	 * 
	 * @param
	 */
	@Override
	public void update(SkillType item) {
		entityManager.merge(item);		
	}


	public List<SkillType> getByUserId(Integer userId) {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}
}
