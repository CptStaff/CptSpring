package com.capgemini.spring.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.models.Project;

/**
 * IProjectDAO interface
 * 
 * @extends
 */
@Repository
public interface IProjectDAO extends IBaseDAO<Project>{

}
