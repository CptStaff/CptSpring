package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.IOwnDAO;
import com.capgemini.spring.models.Own;

/**
 * BaseDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class OwnDAO extends BaseDAO<Own> implements IOwnDAO {

	/**
	 * Create a own
	 * 
	 * @param
	 */
	@Override
	public void create(Own item) {
		entityManager.persist(item);
	}

	/**
	 * Delete a own
	 * 
	 * @param
	 */
	@Override
	public void delete(Own item) {
		entityManager.detach(item);

	}

	/**
	 * List of all owns
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Own> getAll() {
		return entityManager.createQuery("SELECT own FROM Own own").getResultList();
	}

	/**
	 * Get a own by id
	 * 
	 * @param
	 */
	@Override
	public Own getById(Integer id) {
		return entityManager.find(Own.class, id);
	}

	/**
	 * Update a own
	 * 
	 * @param
	 */
	@Override
	public void update(Own item) {
		entityManager.merge(item);
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * Get all owns from one UserId
	 * 
	 * @param
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Own> getByUserId(Integer userId) {
		return entityManager.createQuery("SELECT own FROM Own own WHERE own.user='"+ userId+"'").getResultList();
	}
	
}
