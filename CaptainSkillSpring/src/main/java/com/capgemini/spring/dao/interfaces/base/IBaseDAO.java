package com.capgemini.spring.dao.interfaces.base;

import java.util.List;

import javax.persistence.Query;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * IBaseDAO interface
 * 
 * @abstract
 * @extends
 */
public abstract interface IBaseDAO<T extends BaseEntity> {
	/**
	 * Create an item
	 * 
	 * @param
	 */
	public void create(T item);

	/**
	 * Delete an item
	 * 
	 * @param
	 */
	public void delete(T item);

	/**
	 * List of all items
	 * 
	 */
	public List<T> getAll();

	/**
	 * Get item by id
	 * 
	 * @param
	 */
	public T getById(Integer id);

	/**
	 * Update an item
	 * 
	 * @param
	 */
	public void update(T item);
	

	public Query createQuery(String qlString);
	
	
	public List<T> getByUserId(Integer userId);

}

