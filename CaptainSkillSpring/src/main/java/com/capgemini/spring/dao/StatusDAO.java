package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.IStatusDAO;
import com.capgemini.spring.models.Status;

/**
 * StatusDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class StatusDAO extends BaseDAO<Status> implements IStatusDAO {

	/**
	 * Create a status
	 * 
	 * @param
	 */
	@Override
	public void create(Status item) {
		entityManager.persist(item);
	}

	/**
	 * Delete a status
	 * 
	 * @param
	 */
	@Override
	public void delete(Status item) {
		entityManager.detach(item);

	}

	/**
	 * List of all status
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Status> getAll() {
		return entityManager.createQuery("SELECT status FROM Status status").getResultList();
	}

	/**
	 * Get a status by id
	 * 
	 * @param
	 */
	@Override
	public Status getById(Integer id) {
		return entityManager.find(Status.class, id);
	}

	/**
	 * Update a status
	 * 
	 * @param
	 */
	@Override
	public void update(Status item) {
		entityManager.merge(item);
	}

	@Override

	public List<Status> getByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}
}
