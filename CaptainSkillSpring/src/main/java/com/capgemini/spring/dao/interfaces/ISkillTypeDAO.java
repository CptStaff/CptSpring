package com.capgemini.spring.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.models.SkillType;

/**
 * ISkillTypeDAO interface
 * 
 * @extends
 */
@Repository
public interface ISkillTypeDAO extends IBaseDAO<SkillType> {

}
