package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.ISkillDAO;
import com.capgemini.spring.models.Skill;

/**
 * BaseDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class SkillDAO extends BaseDAO<Skill> implements ISkillDAO{
	/**
	 * Create a skill
	 * 
	 * @param
	 */
	@Override
	public void create(Skill item) {
		entityManager.persist(item);		
	}

	/**
	 * Delete a skill
	 * 
	 * @param
	 */
	@Override
	public void delete(Skill item) {
		entityManager.detach(item);
	}
	
	/**
	 * List of all skills
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Skill> getAll() {
		return entityManager.createQuery("SELECT s FROM Skill s").getResultList();
	}

	/**
	 * Get a skill by id
	 * 
	 * @param
	 */
	@Override
	public Skill getById(Integer id) {
		return entityManager.find(Skill.class, id);
	}

	/**
	 * Update a skill
	 * 
	 * @param
	 */
	@Override
	public void update(Skill item) {
		entityManager.merge(item);		
	}


	public List<Skill> getByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}
}
