package com.capgemini.spring.dao.interfaces;

import org.springframework.stereotype.Repository;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.models.GeneralSkillType;

/**
 * IGeneralSkillTypeDAO interface
 * 
 * @extends
 */
@Repository
public interface IGeneralSkillTypeDAO extends IBaseDAO<GeneralSkillType>{

}
