package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.IUserDAO;
import com.capgemini.spring.models.User;

/**
 * UserDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class UserDAO extends BaseDAO<User> implements IUserDAO{
	/**
	 * Create a user
	 * 
	 * @param
	 */
	@Override
	public void create(User item) {
		entityManager.persist(item);
	}

	/**
	 * Delete a user
	 * 
	 * @param
	 */
	@Override
	public void delete(User item) {
		entityManager.detach(item);
	}
	
	/**
	 * List of all users
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAll() {
		return entityManager.createQuery("SELECT u FROM User u").getResultList();
	}

	/**
	 * Get a user by id
	 * 
	 * @param
	 */
	@Override
	public User getById(Integer id) {
		return entityManager.find(User.class, id);
	}

	/**
	 * Update a user
	 * 
	 * @param
	 */
	@Override
	public void update(User item) {
		entityManager.merge(item);	
	}

	
	public User getByEmail(String email) {
		return (User) this.entityManager.createQuery("SELECT u FROM User u WHERE u.email=:email").setParameter("email", email).setMaxResults(1).getSingleResult();
	}


	@Override
	public List<User> getByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		return this.entityManager.createQuery(qlString);
	}


}
