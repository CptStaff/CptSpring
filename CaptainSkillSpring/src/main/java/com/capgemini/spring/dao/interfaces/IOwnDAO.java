package com.capgemini.spring.dao.interfaces;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.spring.dao.interfaces.base.IBaseDAO;
import com.capgemini.spring.models.Own;

/**
 * IOwnDAO interface
 * 
 * @extends
 */
@Repository
public interface IOwnDAO extends IBaseDAO<Own>{

	List<Own> getByUserId(Integer userId);

}
