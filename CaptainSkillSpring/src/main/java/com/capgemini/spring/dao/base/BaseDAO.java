package com.capgemini.spring.dao.base;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.models.base.BaseEntity;

/**
 * BaseDAO class
 * 
 * @abstract
 * @extends
 */
@Service
@Transactional
public abstract class BaseDAO<T extends BaseEntity> {

	/**
	 * Manage the DAO
	 * 
	 */
	@PersistenceContext
	protected EntityManager entityManager;
}