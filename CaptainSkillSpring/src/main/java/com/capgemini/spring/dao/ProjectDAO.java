package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.IProjectDAO;
import com.capgemini.spring.models.Project;
import com.capgemini.spring.models.Skill;

/**
 * BaseDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class ProjectDAO extends BaseDAO<Project> implements IProjectDAO{

	/**
	 * Create a project
	 * 
	 * @param
	 */
	@Override
	public void create(Project item) {
		entityManager.persist(item);
		
	}
	
	/**
	 * Delete a project
	 * 
	 * @param
	 */
	@Override
	public void delete(Project item) {
		entityManager.detach(item);
		
	}

	/**
	 * List of all project
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Project> getAll() {
		return entityManager.createQuery("SELECT p FROM Project p").getResultList();
	}
	

	/**
	 * Get a project by id
	 * 
	 * @param
	 */
	@Override
	public Project getById(Integer id) {
		return entityManager.find(Project.class, id);
	}

	/**
	 * Update a project
	 * 
	 * @param
	 */
	@Override
	public void update(Project item) {
		entityManager.merge(item);
		
	}

	
	
	public List<Project> getByUserId(Integer userId) {

		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}

}
