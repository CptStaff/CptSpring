package com.capgemini.spring.dao;

import java.util.List;

import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.capgemini.spring.dao.base.BaseDAO;
import com.capgemini.spring.dao.interfaces.IGeneralSkillTypeDAO;
import com.capgemini.spring.models.GeneralSkillType;

/**
 * GeneralSkillTypeDAO class
 * 
 * @extends
 * @implements
 */
@Transactional
public class GeneralSkillTypeDAO extends BaseDAO<GeneralSkillType> implements IGeneralSkillTypeDAO {
	/**
	 * Create a general skill type
	 * 
	 * @param
	 */
	@Override
	public void create(GeneralSkillType item) {
		entityManager.persist(item);		
	}

	/**
	 * Delete a general skill type
	 * 
	 * @param
	 */
	@Override
	public void delete(GeneralSkillType item) {
		entityManager.detach(item);
		
	}

	/**
	 * List of all general skill types
	 * 
	 * @param
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GeneralSkillType> getAll() {
		return entityManager.createQuery("SELECT gst FROM GeneralSkillType gst ORDER BY id ASC").getResultList();
	}

	/**
	 * Get a general skill type by id
	 * 
	 * @param
	 */
	@Override
	public GeneralSkillType getById(Integer id) {
		return entityManager.find(GeneralSkillType.class, id);
	}

	/**
	 * Update a general skill type
	 * 
	 * @param
	 */
	@Override
	public void update(GeneralSkillType item) {
		entityManager.merge(item);		
	}



	public List<GeneralSkillType> getByUserId(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Query createQuery(String qlString) {
		// TODO Auto-generated method stub
		return null;
	}
}
